
## Every VM must be configured with domain names for both public and private IPs as such:

* Public IP domain: ```vm.wicc.me```
* Private IP domain: ```vmi.wicc.me```

## Kong configuration
* Upstream server IP
** Must use VM/VMI domain names