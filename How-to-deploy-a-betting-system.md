## Disclaimer
* This is a deployment guide only for Waykichain Betting System v2.0. There may be huge changes in the upcomping releases. Stay attuned!
* Waykichain will not be liable for any damage or loss to the people involved in implementing or using the software.

## Installation Steps
1. Install Hot wallet
1. Install Database
1. Install Backend Web API
1. Install Admin Front-end WebUI
1. Build and Install App (Android/iOS)