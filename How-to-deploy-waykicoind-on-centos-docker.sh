#!/bin/bash

sudo yum check-update
#install docker
curl -fsSL https://get.docker.com/ | sh
#start docker service
sudo systemctl start docker
# verify if the docker is up & running
sudo systemctl status docker
sudo systemctl enable docker
sudo usermod -aG docker $(whoami)

# below installs WaykiChain node program in a docker
docker pull wicc/waykicoind
sudo mkdir -p /opt/docker-instances/wicc-wallet-main/data
sudo mkdir -p /opt/docker-instances/wicc-wallet-main/conf
sudo mkdir -p /opt/docker-instances/wicc-wallet-main/bin
sudo wget -P /opt/docker-instances/wicc-wallet-main/conf https://raw.githubusercontent.com/WaykiChain/docker-waykicoind/master/WaykiChain.conf
sudo wget -P /opt/docker-instances/wicc-wallet-main/bin https://raw.githubusercontent.com/WaykiChain/docker-waykicoind/master/bin/run-waykicoind-main.sh

# starts WaykiChain node in a docker container
cd /opt/docker-instances/wicc-wallet-main && sh ./bin/run-waykicoind-main.sh