|module|usage|
|--|--|
|bench | The code to do benchmarking. |
|compat | The code to make it compatible with different building environment (OS, compiler...).|
|config | Initially it is empty. All configurations will be generated during building and a file named "bitcoin-config.h" will be put here.|
|consensus | The code to make Bitcoin nodes reaching consensus, including implementations of merkle trees and transactions verification.|
|crypto | The code to handle all cryptography related operations, including SHA256, AES, RIPEMD160(the method to generate bitcoin's 160-bit address) and so on.|
|leveldb | The key/value database. Bitcoin uses leveldb to store blocks info locally.|
|obj | The folder to store compiled objects.|
|obj-test | The folder to store compiled objects of test cases.|
|policy | Policies used by the wallet.|
|primitives | The definition of blocks and transactions.|
|qt | The UI part using QT library.|
|rpc | The implementation of remote process call interfaces.|
|script | The Bitcoin script language handler.|
|secp256k1 | A 3rd party optimized C library for EC operations on curve secp256k1.|
|support | Some supporting/helper functions.|
|test | The unit test cases of each module.|
|univalue | A 3rd party universal value object and JSON library.|
|wallet | The bitcoin wallet implementation.|
|zmq | Zeromq (http://zeromq.org/). The 3rd party distributed messaging queue implementation.|
| | |
|addrdb.cpp | Read/write peers info.|
|base58.cpp | Bitcoin's base58 encoding/decoding.|
|bloom.cpp| The bloom filters implementation.|
|bitcoin-cli.cpp |  The command line interface tool.|
|bitcoin-tx.cpp | The bitcoin trasaction tool.|
|bitcoind.cpp | The main entry of the program.|
|chain.cpp | The blockchain implementation.|
|txdb.cpp | The transactions.|
|txmempool.cpp | The transactions pool in memeory.|
|validation.cpp | The verification on blocks.|
| | |

### run program in debug mode
```$ bitcoind -printtoconsole -debug```