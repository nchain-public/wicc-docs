mylib = require "mylib"

_G.Context = {
  ADDR_TYPE = {
    REGID  = 1,
    BASE58 = 2
  },
  OP_TYPE = {
    ADD_FREE 	= 1,
    SUB_FREE 	= 2
  },
  LIFE_TYPE = {
    LIFE_DEAD = 0x00,
    LIFE_ALIVE = 0x01
  },
  Init=function()
      if _G._C == nil then _G._C  = _G.Context end
  end,
  TableIsNotEmpty = function (t)
    return _G.next(t) ~= nil
  end,
  Unpack = function (t,i)
    i = i or 1
    if t[i] then
      return t[i], _G._C.Unpack(t,i+1)
    end
  end,
  LogMsg = function (msg)
    _G.mylib.LogPrint({0, string.len(msg), msg})
  end,
  GetContractValue = function (key)
    assert(#key > 1, "Key is empty")

    local tValue = { _G.mylib.ReadData(key) }
    if _G._C.TableIsNotEmpty(tValue) then
      return true,tValue
    else
      _G._C.LogMsg("Key not exist")
      return false,nil
    end
  end,
  GetContractTxParam = function (startIndex, length)
    assert(startIndex > 0, "GetContractTxParam start error(<=0).")
    assert(length > 0, "GetContractTxParam length error(<=0).")
    assert(startIndex+length-1 <= #_G.contract, "GetContractTxParam length ".. length .." exceeds limit: " .. #_G.contract)

    local newTbl = {}
    local i = 1
    for i = 1,length do
      newTbl[i] = _G.contract[startIndex+i-1]
    end
    return newTbl
  end,
  MemIsEqual = function (t1,t2)
    assert(_G._C.TableIsNotEmpty(t1), "t1 is empty")
    assert(_G._C.TableIsNotEmpty(t2), "t2 is empty")
    assert(#t1 == #t2, "t1 vs t2 size different")

    local i = 1
    for i = #t1,1,-1 do
      if t1[i] ~= t2[i] then
        return false
      end
    end
    return true
  end,
  WriteOrModify = function (isConfig, writeTbl)
    if not isConfig then
      if not _G.mylib.WriteData(writeTbl) then	error("FuncSetAdmin: Write error") end
    else
      if not _G.mylib.ModifyData(writeTbl) then error("FuncSetAdmin: Modify error") end
    end
  end,
  WriteAppData = function (opType, moneyTbl, userIdTbl)
    local appOperateTbl = {
      operatorType = opType,
      outHeight = 0,
      moneyTbl = moneyTbl,
      userIdLen = #userIdTbl,
      userIdTbl = userIdTbl,
      fundTagLen = 0,
      fundTagTbl = {}
    }
    assert(_G.mylib.WriteOutAppOperate(appOperateTbl), "WriteAppData: ".. opType .." op err")
  end,
  WriteAccountData = function (opType, addrType, accountIdTbl, moneyTbl)
    local writeOutputTbl = {
      addrType = addrType,
      accountIdTbl = accountIdTbl,
      operatorType = opType,
      outHeight = 0,
      moneyTbl = moneyTbl
    }
    assert(_G.mylib.WriteOutput(writeOutputTbl),"WriteAccountData" .. opType .. " err")
  end,
  WriteAccountWithdraw = function (addrType, accTbl, moneyTbl)
    assert(_G._C.TableIsNotEmpty(accTbl), "WriteWithdrawal accTbl empty")
    assert(_G._C.TableIsNotEmpty(moneyTbl), "WriteWithdrawal moneyTbl empty")
    _G._C.WriteAccountData(_G._C.OP_TYPE.ADD_FREE, addrType, accTbl, moneyTbl)
    local appRegId = {_G.mylib.GetScriptID()}
    _G._C.WriteAccountData(_G._C.OP_TYPE.SUB_FREE, _G._C.ADDR_TYPE.REGID, appRegId, moneyTbl)
    return true
  end,
  GetCurrTxAccountAddress = function ()
    local accountTbl = { _G.mylib.GetCurTxAccount() }
    return {_G.mylib.GetBase58Addr(_G._C.Unpack(accountTbl))}
  end,
  GetCurrTxPayAmount = function ()
    local tblPayMoney = {_G.mylib.GetCurTxPayAmount()}
    local payMoney = _G.mylib.ByteToInteger(_G._C.Unpack(tblPayMoney))
    assert(payMoney > 0,"GetCurrTxPayAmount: payMoney <= 0")
    return payMoney
  end,
  GetFreeTokenCount = function (accountTbl)
    local freeMoneyTbl = { _G.mylib.GetUserAppAccValue( {idLen = #accountTbl, idValueTbl = accountTbl} ) }
    assert(_G._C.TableIsNotEmpty(freeMoneyTbl), "GetUserAppAccValue error")
    return _G.mylib.ByteToInteger(_G._C.Unpack(freeMoneyTbl))
  end,
  GetTokenExchangeRate = function ()
    local isConfig,rateTbl = _G._C.GetContractValue(_G.WTB.gKeys[_G.WTB.gKeysIndex.TOKEN_RATE])
    if not isConfig then error("Token Exchange Rate not set error") end
    return _G.mylib.ByteToInteger(_G._C.Unpack(rateTbl))
  end,
  TransferToken = function (fromAddrTbl, toAddrTbl, moneyTbl)
    local money = _G.mylib.ByteToInteger(_G._C.Unpack(moneyTbl))
    assert(money > 0, money .. " <=0 error")
    local freeMoneyTbl = { _G.mylib.GetUserAppAccValue({idLen = #fromAddrTbl, idValueTbl = fromAddrTbl}) }
    assert(_G._C.TableIsNotEmpty(freeMoneyTbl), "GetUserAppAccValue error")
    local freeMoney = _G.mylib.ByteToInteger(_G._C.Unpack(freeMoneyTbl))
    assert(freeMoney >= money, "Insufficient money to transfer in the account.")

    _G._C.WriteAppData( _G._C.OP_TYPE.SUB_FREE, moneyTbl, fromAddrTbl )
    _G._C.WriteAppData( _G._C.OP_TYPE.ADD_FREE, moneyTbl, toAddrTbl )
  end,
  TransferTokenWithFee = function (adminAccountTbl, start)
    local index = start or 5
    local fromTbl = _G._C.GetContractTxParam(index, 34)
    index = index + 34
    local toTbl = _G._C.GetContractTxParam(index, 34)
    index = index + 34
    local moneyTbl = _G._C.GetContractTxParam(index, 8)
    index = index + 8
    local feeTbl = _G._C.GetContractTxParam(index, 8)
    local freeMoney = _G._C.GetFreeTokenCount(fromTbl)
    local money = _G.mylib.ByteToInteger(_G._C.Unpack(moneyTbl))
    local fee = _G.mylib.ByteToInteger(_G._C.Unpack(feeTbl))
    assert(freeMoney >= (money+fee), "The addr balance is insufficient")

    _G._C.TransferToken(fromTbl, toTbl, moneyTbl)
    if (fee>0) then _G._C.TransferToken(fromTbl, adminAccountTbl, feeTbl) end
  end,
  ProcessTransferToken = function (pos)
    local index = pos or 5
    local fromTbl = _G._C.GetContractTxParam(index, 34)
    index = index + 34
    local toTbl = _G._C.GetContractTxParam(index, 34)
    index = index + 34
    local moneyTbl = _G._C.GetContractTxParam(index, 8)
    _G._C.TransferToken(fromTbl, toTbl, moneyTbl)
  end
}

_G.WTB={
  gKeys = { "key_contractalive", "key_superadmin", "key_tokenadmin", "key_gameadmin", "key_lottadmin", "key_tokenswitch", "key_tokenrate" },
  gKeysIndex = { CONTRACT_ALIVE = 1, SUPER_ADMIN = 2, TOKEN_ADMIN = 3, GAME_ADMIN = 4, LOTT_ADMIN = 5, TOKEN_SWITCH = 6, TOKEN_RATE = 7 },
  gSuperAdminAddr = { 0x57,0x5a,0x73,0x77,0x71,0x72,0x66,0x68,0x75,0x32,0x70,0x58,0x68,0x39,0x63,0x7a,0x6d,0x70,0x36,0x48,0x69,0x68,0x4b,0x36,0x68,0x64,0x4b,0x78,0x34,0x33,0x78,0x68,0x6b,0x6a },
  Init = function()
    _G.WTB[0x01] = _G.WTB.FuncSetAlive
    _G.WTB[0x11] = _G.WTB.FuncSetAdmin
    _G.WTB[0x12] = _G.WTB.FuncSetTokenSwitch
    _G.WTB[0x13] = _G.WTB.FuncSetTokenRate
    _G.WTB[0x14] = _G.WTB.FuncExchangeForToken
    _G.WTB[0x15] = _G.WTB.FuncExchangeForWicc
    _G.WTB[0x16] = _G.WTB.FuncOwnerTransferToken
    _G.WTB[0x17] = _G.WTB.FuncAdminTransferToken
    _G.WTB[0x18] = _G.WTB.FuncFreezeToken
    _G.WTB[0x19] = _G.WTB.FuncUnfreezeToken

    _G.WTB[0x21] = _G.WTB.FuncGameCreate
    _G.WTB[0x22] = _G.WTB.FuncGameCreateAndBet
    _G.WTB[0x23] = _G.WTB.FuncGameUpdate
    _G.WTB[0x24] = _G.WTB.FuncGameUpdateAndBet
    _G.WTB[0x25] = _G.WTB.FuncGameBet
    _G.WTB[0x26] = _G.WTB.FuncGameBetBatch
    _G.WTB[0x27] = _G.WTB.FuncGameBetClose
    _G.WTB[0x28] = _G.WTB.FuncGameSendPrize
    _G.WTB[0x29] = _G.WTB.FuncGameSendPrizeBatch

    _G.WTB[0x41] = _G.WTB.FuncLotteryBet
    _G.WTB[0x42] = _G.WTB.FuncLotteryDraw
    _G.WTB[0x43] = _G.WTB.FuncLotteryBetLog
    _G.WTB[0x44] = _G.WTB.FuncLotteryDrawLog
  end,
  CheckIsSuperAdmin = function ()
    local txAccountAddr = _G._C.GetCurrTxAccountAddress()
    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.SUPER_ADMIN]
    local isConfig,addr = _G._C.GetContractValue(key)
    if not isConfig then
      if not _G._C.MemIsEqual(txAccountAddr, _G.WTB.gSuperAdminAddr) then error("Neither Owner nor Super Admin error") end
    elseif not _G._C.MemIsEqual(txAccountAddr, addr) then error("False Super Admin error")
    end
  end,
  CheckIsAdmin = function (adminType)
    local key = _G.WTB.gKeys[adminType]
    if key == nil then error("Admin type error") end
    local isConfig,adminAddr = _G._C.GetContractValue(key)
    if not isConfig then error("Admin not set error") end
    local txAccountAddr = _G._C.GetCurrTxAccountAddress()
    if not _G._C.MemIsEqual(txAccountAddr, adminAddr) then error("Fake admin error")	end
    return adminAddr
  end,
  CheckIsAlive = function ()
    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.CONTRACT_ALIVE]
    local isConfig,aliveTbl = _G._C.GetContractValue(key)
    if not isConfig then return end
    local isAlive = aliveTbl[1]
    if isAlive == _G._C.LIFE_TYPE.LIFE_DEAD then error("CONTRACT DEAD!!!") end
  end,
   FuncSetAlive = function ()
    _G.WTB.CheckIsSuperAdmin()

    local aliveTbl = _G._C.GetContractTxParam(5, 1)
    local alive = aliveTbl[1]
    if alive ~= _G._C.LIFE_TYPE.LIFE_ALIVE and alive ~= _G._C.LIFE_TYPE.LIFE_DEAD then error("Life Type error: " .. alive) end
    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.CONTRACT_ALIVE]
    local isConfig,_ = _G._C.GetContractValue(key)
    _G._C.WriteOrModify(isConfig, {key = key, length = 1, value = aliveTbl})
  end,
  FuncSetAdmin = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsSuperAdmin()

    local adminAddress = _G._C.GetContractTxParam(5, 34)
    local adminTypeTbl = _G._C.GetContractTxParam(39, 1)
    local adminType = _G._C.Unpack(adminTypeTbl)
    if adminType < 2 or adminType > 5 then error("FuncSetAdmin: admin type error: " .. adminType ) end
    local key = _G.WTB.gKeys[adminType]
    if key == nil then error("FuncSetAdmin: admin type error") end
    local isConfig,_ = _G._C.GetContractValue(key)
    _G._C.WriteOrModify(isConfig, {key=key, length=34, value=adminAddress})
  end,
  FuncSetTokenSwitch = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)

    local tokenSwitch = _G._C.GetContractTxParam(5,1)
    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.TOKEN_SWITCH]
    local isConfig,_ = _G._C.GetContractValue(key)
    _G._C.WriteOrModify(isConfig, {key=key, length=1, value=tokenSwitch})
  end,
  FuncSetTokenRate = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)

    local exchangeRate = _G._C.GetContractTxParam(5,8)
    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.TOKEN_RATE]
    local isConfig,_ = _G._C.GetContractValue(key)
    _G._C.WriteOrModify(isConfig, {key=key, length=8, value=exchangeRate})
  end,
  FuncExchangeForToken = function ()
    _G.WTB.CheckIsAlive()

    local txAccountAddr = _G._C.GetCurrTxAccountAddress()
    local txPayAmount = _G._C.GetCurrTxPayAmount()

    local pos = 5
    local inputExchangeRateTbl = _G._C.GetContractTxParam(pos, 8)
    local exchagneRateValue = _G.mylib.ByteToInteger(_G._C.Unpack(inputExchangeRateTbl))
    local configExchangeRate = _G._C.GetTokenExchangeRate()
    assert(exchagneRateValue<=configExchangeRate, "FuncExchangeForToken: " .. configExchangeRate .. " vs " .. exchagneRateValue)

    local tokenCount =  txPayAmount * configExchangeRate / 10000

    pos = pos + 8
    local inputTokenCountTbl = _G._C.GetContractTxParam(pos, 8)
    local inputTokenCount = _G.mylib.ByteToInteger(_G._C.Unpack(inputTokenCountTbl))
    assert(inputTokenCount<=tokenCount, "FuncExchangeForToken: tokenCount" ..  "vs " .. inputTokenCount)

    _G._C.WriteAppData(_G._C.OP_TYPE.ADD_FREE, inputTokenCountTbl, txAccountAddr)
  end,
  FuncExchangeForWicc = function ()
    _G.WTB.CheckIsAlive()
    local adminAddrTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)

    local pos = 5
    local addrParam = _G._C.GetContractTxParam(pos, 34)

    pos = pos + 34
    local moneyParam = _G._C.GetContractTxParam(pos, 8)
    local money = _G.mylib.ByteToInteger(_G._C.Unpack(moneyParam))
    assert(money > 0, money .. " <= 0 ")
    local freeMoney = _G._C.GetFreeTokenCount(addrParam)
    assert(freeMoney >= money, freeMoney .. " < " .. money)

    pos = pos + 8
    local inputExchangeRateTbl = _G._C.GetContractTxParam(pos, 8)
    local exchagneRateValue = _G.mylib.ByteToInteger(_G._C.Unpack(inputExchangeRateTbl))
    local configExchangeRate = _G._C.GetTokenExchangeRate()
    assert(exchagneRateValue>=configExchangeRate, exchagneRateValue .. " < " .. configExchangeRate)

    local wiccCountValue =  money * 10000 / configExchangeRate

    pos = pos + 8
    local inputWiccCountTbl = _G._C.GetContractTxParam(pos, 8)
    local inputWiccCount = _G.mylib.ByteToInteger(_G._C.Unpack(inputWiccCountTbl))
    assert(inputWiccCount <= wiccCountValue, inputWiccCount .. " > " .. wiccCountValue)

    _G._C.WriteAppData(_G._C.OP_TYPE.SUB_FREE, moneyParam, addrParam)
    _G._C.WriteAccountWithdraw(_G._C.ADDR_TYPE.BASE58, addrParam, inputWiccCountTbl)

    pos = pos + 8
    local feeTbl = _G._C.GetContractTxParam(pos, 8)
    local fee = _G.mylib.ByteToInteger(_G._C.Unpack(feeTbl))
    if (fee>0) then _G._C.TransferToken(addrParam, adminAddrTbl, feeTbl) end
  end,

  FuncOwnerTransferToken = function ()
    _G.WTB.CheckIsAlive()

    local key = _G.WTB.gKeys[_G.WTB.gKeysIndex.TOKEN_SWITCH]
    local isConfig,tValue = _G._C.GetContractValue(key)
    if isConfig and tValue[1] ~= 0 then error("TokenSwitch is on: transfer denied!") end
    local pos = 5
    local targetAddr = _G._C.GetContractTxParam(pos, 34)
    pos = pos + 34
    local tMoney = _G._C.GetContractTxParam(pos, 8)
    local ownerAddr = _G._C.GetCurrTxAccountAddress()
    _G._C.TransferToken(ownerAddr, targetAddr, tMoney)
  end,
  FuncAdminTransferToken = function ()
    _G.WTB.CheckIsAlive()
    local tokenAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)
    _G._C.TransferTokenWithFee(tokenAdminTbl)
  end,
  FuncFreezeToken = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)
    _G._C.ProcessTransferToken()
  end,
  FuncUnfreezeToken = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.TOKEN_ADMIN)
    _G._C.ProcessTransferToken()
  end,
  FuncGameCreate = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)
    _G._C.TransferTokenWithFee(gameAdminTbl)
  end,
  FuncGameCreateAndBet = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)
    _G._C.TransferTokenWithFee(gameAdminTbl)
    local pos = 89
    _G._C.TransferTokenWithFee(gameAdminTbl, pos)
  end,
  FuncGameUpdate = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)

    local pos = 5
    local fromTbl = _G._C.GetContractTxParam(pos, 34)
    pos = pos + 34
    local feeTbl = _G._C.GetContractTxParam(pos, 8)
    local fee = _G.mylib.ByteToInteger(_G._C.Unpack(feeTbl))
    if(fee>0) then _G._C.TransferToken(fromTbl, gameAdminTbl, feeTbl) end
  end,
  FuncGameUpdateAndBet = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)

    local pos = 89
    local fromTbl = _G._C.GetContractTxParam(pos, 34)
    pos = pos + 34
    local feeTbl = _G._C.GetContractTxParam(pos, 8)
    local fee = _G.mylib.ByteToInteger(_G._C.Unpack(feeTbl))
    if (fee>0) then _G._C.TransferToken(fromTbl, gameAdminTbl, feeTbl) end

    _G._C.TransferTokenWithFee(gameAdminTbl)
  end,
  FuncGameBetClose = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)
    _G._C.TransferTokenWithFee(gameAdminTbl)
  end,
  FuncGameBet = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)
    _G._C.TransferTokenWithFee(gameAdminTbl)
  end,
  FuncGameBetBatch = function ()
    _G.WTB.CheckIsAlive()
    local gameAdminTbl = _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)

    local pos = 5
    local betCountTbl = _G._C.GetContractTxParam(pos, 4)
    local betCount = _G.mylib.ByteToInteger(_G._C.Unpack(betCountTbl))
    assert(betCount>=1, "Error: betCount < 1")

    pos = pos + 4
    local i
    for i=1, betCount do
      _G._C.TransferTokenWithFee(gameAdminTbl,pos)
      pos = pos + 124
    end

  end,
  FuncGameSendPrize = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)

    local pos = 5
    pos = pos + 20
    local fromTbl = _G._C.GetContractTxParam(pos, 34)

    pos = pos + 34
    local sendPrizeCountTbl = _G._C.GetContractTxParam(pos, 4)
    local sendPrizeCount = _G.mylib.ByteToInteger(_G._C.Unpack(sendPrizeCountTbl))
    assert(sendPrizeCount>=1, "Error: sendPrizeCount < 1")

    pos = pos + 4
    local i
    for i=1, sendPrizeCount do
      local toTbl = _G._C.GetContractTxParam(pos, 34)
      pos = pos + 34
      local tokenCountTbl = _G._C.GetContractTxParam(pos, 8)
      _G._C.TransferToken(fromTbl, toTbl, tokenCountTbl)
      pos = pos + 8
    end
  end,
  FuncGameSendPrizeBatch = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.GAME_ADMIN)

    local pos = 5
    pos = pos + 20
    local sendPrizeCountTbl = _G._C.GetContractTxParam(pos, 4)
    local sendPrizeCount = _G.mylib.ByteToInteger(_G._C.Unpack(sendPrizeCountTbl))
    assert(sendPrizeCount>=1, "Error: sendPrizeCount < 1")

    pos = pos + 4
    local i
    for i=1, sendPrizeCount do
      _G._C.ProcessTransferToken(pos)
    pos = pos + 76
    end
  end,
  FuncLotteryBet = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.LOTT_ADMIN)
    _G._C.ProcessTransferToken()
  end,
  FuncLotteryDraw = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.LOTT_ADMIN)
    _G._C.ProcessTransferToken()
  end,
  FuncLotteryBetLog = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.LOTT_ADMIN)
  end,
  FuncLotteryDrawLog = function ()
    _G.WTB.CheckIsAlive()
    _G.WTB.CheckIsAdmin(_G.WTB.gKeysIndex.LOTT_ADMIN)
  end
}
_G.Context.Main = function()
  assert(#_G.contract >=4, "Param length error (<4): " ..#_G.contract )
  assert(_G.contract[1] == 0xf0, "Param MagicNo error (~=0xf0): " .. _G.contract[1])

  _G.Context.Init()
  local callFuncNum = _G.contract[2]
  local callFun = _G.WTB[callFuncNum]
  if callFun == nil then error('method# '..string.format("%02x", callFuncNum)..' not found') end
  callFun()
end

_G.WTB.Init()
_G.Context.Main()