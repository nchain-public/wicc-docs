/* 
  Localizable.strings
  CommApp

  Created by sorath on 2018/10/12.
  Copyright © 2018年 sorath. All rights reserved.
*/
"localLanguage" = "2";//系统语言

/// 资产
"总资产" = "Total Assets";
"转账" = "Send";
"收款" = "Receive";
"闪兑" = "Instant Trade";
"钱包" = "Wallet";
"WICC及您在维基链的所有资产" = "WICC/WGRT etc. assets in your wallet";
"借贷" = "Colllateral Loans";
"质押WICC,贷出WUSD" = "Pledge WICC to lend WUSD";
"Wayki-X" = "Wayki-X";
"每周获取XT增发奖励" = "Claim Weekly Infation Rewards";
"锁仓" = "Escrow";
"理财" = "Wealth Management";
"抵押维基链底层币，年化收益20%" = "Stake WICC coins to get interest of 20% APR";
"合成资产" = "Synthetic Assets";

"￥" = "$";

/// 借贷
"进入" = "Enter";
"抵押率" = "C-RATIO";
"已抵押" = "Pledged";
"已贷出" = "Borrowed";
"清算价" = "Liquidation price";

/// Wayki-X
"待偿还债务" = "Outstanding debt";
"已锁仓奖励" = "Rewards under escrow";
"未领取奖励" = "Rewards to be claimed";
"已抵押资产" = "Pledged assets";

/// 创建钱包页
"创建钱包" = "Create Account";
"没有WICC钱包" = "No WICC account";
"已有WICC钱包" = "WICC acccount exists";
"导入钱包" = "Import Account";

/// 闪兑
"钱包密码" = "Wallet Password";
"矿工费" = "Miner fees";
"请输入" = "Please enter";
"确认" = "Confirm";
"闪电兑换" = "Instand Trade";
"转出数量" = "Send amount";
"收到数量" = "Receive amount";
"余额" = "Balance";
"余额:￥" = "Balance: $";
"余额:" = "Balance:";
"汇率" = "Exchange rate";
"前往DEX进行挂单交易" = "Go to DEX to place orders";
"请输入正确的密码" = "Please enter the correct password";

/// 我的
"服务于隐私条款" = "Privacy Policy";
"关于维基" = "About WaykiChain";
"版本" = "Version";
"钱包管理" = "Wallet Management";
"帮助与反馈" = "Feedback & Support";
"设置" = "Settings";

/// tabbars
"我的" = "Me";
"发现" = "Discover";
"交易" = "Trade";
"资产" = "Assets";

/// 交易
"合成" = "Synths";
"币币" = "DEX";
"什么是合成资产" = "What are synthetic assets?";
"数字货币" = "Crypto currency";
"ETF指数" = "ETF index";
"外汇指数" = "Forex Index";
"美股" = "US Stocks";
"港股" = "HK Stocks";
"名称" = "Name";
"涨跌幅" = "Chg%";
"最新价" = "Price";
"我要买xUSD" = "Buy xUSD";

"获取您的资产信息" = "Access your asset information";
"获取您的钱包信息" = "Access your wallet information";
"获取您的Wayki-X信息" = "Get your Wayki-X information";
"获取您的借贷信息" = "Get your debt and credit information";
"加载中..." = "Loading...";
"请前往DEX挂单交易" = "Please go to DEX for order book";

"已复制" = "Copied";


"资讯" = "News";
"社区" = "Community";
"我" = "Me";

//弹框
"网络已关闭" = "No Internet accesss";
"请打开网络设置，确保设备联网" = "Please turn on your network to make sure your device is connected";
"网络连接失败，请稍后再试" = "Network connection failed, please try again later";

"取消" = "Cancel";
"扫一扫" = "Scan";
"关闭" = "Close";
"相册" = "Album";
"拍照" = "Take a photo";
"选择图片" = "Select Images";

"无法扫描到二维码，请重试" = "Unable to scan QR code, please try again";
"保存成功" = "Save succeeded";
"保存失败" = "Save failed";

"备注" = "Memo";
"无" = "--";
"确定" = "Confirm";
"提示" = "Reminder";
"尚未开启相机权限,您可以去设置中开启" = "Camera privileges not yet granted. Please go to Settings to enable them";
"尚未开启相册权限,您可以去设置中开启" = "Album permissions not granted. Please go to Settings to enable them";

"版本更新" = "Version upgrade";
"更新" = "Upgrade";
"调用地址" = "Address";
"合约描述" = "Contract Description";
"合约内容" = "Contract Content";
"(您可以复制此regid到区块链浏览器查询该应用)" = "(You can copy this RegID to the blockchain explorer and search for the application)";
"合约RegID" = "Contract RegID";
"合约字段" = "Contract";
"转入金额" = "Send Amount";
"备注" = "Memo";
"被投票地址" = "Address";
"投票数量" = "Votes";
"备注" = "Memo";
"付款地址" = "From";
"收款地址" = "To";
"合约调用" = "Invoke Contract";
"合约发布" = "Deploy Contract";
"转账确认" = "Transfer";
"节点投票" = "Vote Delegate";
"拒绝" = "Cancel";
"确定" = "Confirm";
"矿工费：" = "Miner fees:";
"慢" = "Slow";
"快" = "Fast";
"请输入钱包密码" = "Enter your wallet  password";
"请输入维基钱包密码" = "Enter your wallet  password";
"密码错误，请重新输入" = "The password you entered is incorrect.";

"点击拒绝" = "Click to reject transaction signing";
"点击取消" = "Click to cancel transaction signature.";
"投票地址数量不能超过11个" = "Cannot vote more than 11 delegates";


"您在第三方DApp上的使用行为将适用该第三方DApp的《用户协议》和《隐私政策》，由该第三方DApp直接向您承担责任。" = "By using the third-party DApp, you are bound by the <User Agreement> and <Privacy Policy> of the third-party DApp, and the third-party DApp will be responsible for you directly.";
"分享" = "Share";
"复制链接" = "Copy Link";
"法律声明" = "Legal Notice";
"知道了" ="Noted";
"发帖" = "Post";
"保存到本地" = "Download";


"维基链第三方应用法律声明" = "Legal Notice";
"法律声明内容" = "With WaykiChain’s team actively building the public chain ecosystem, the performance of the public chain has been greatly improved. WaykiChain is able to sustain transactional throughput above 4,500 TPS in real cases. The developer-friendly tools and services are constantly getting enriched and developed. As a result, a batch of new applications have emerged on WaykiChain recently, and the public chain has shown its growing vitality. We believe that there will be various types of DApps launching on WaykiChain in the future. Given that these DApps are developed and operated by third parties based on WaykiChain’s public chain, a broad range of aspects might be covered. Potential risks exist and cannot be secure. WaykiChain hereby certify that:\n(1)WaykiChain is a public chain. Conforming with Ethereum and other blockchain companies, WaykiChain allows individuals and companies worldwide to develop and operate applications after the API interface opening(hereinafter referred to as third-party applications).\n(2)Third-party applications or website developed and operated based on WaykiChain public chain shall comply with local laws and policies. WaykiChain official does not guarantee effective supervision or interference over the operation of third-party applications, due to the limitation of capabilities. Therefore, WaykiChain official does not guarantee the legality of third-party applications.\n(3)The fairness, justness, and transparency advocated by WaykiChain are realized by the characteristics of public blockchain technology, not by the supervision from WaykiChain Official or any other centralized organizations. After opening its API, WaykiChain Official is unable to control the behavior of third parties. Third parties will bear the legal responsibility by themselves. WaykiChain shall not be responsible for the behavior, or provide any kind of guarantee or promise for the legality of the third parties. However, if third parties' actions are suspected of violating the laws or infringing other users' rights, WaykiChain reserve the right to cooperate with local authorities in accordance with law.\n(4)When users use applications developed on WaykiChain, they should be in compliance with local laws and policies of the country or region they located. If the users' participation are prohibited or restricted by relevant laws or policies, the users should not continue to use the applications. WaykiChain Official shall not bear any responsibility for the legality of users' behavior or provide any guarantee or promise.";


"测试" = "Test";
"请先激活钱包。" = "Please create your account first.";
"请先创建或者导入钱包。" = "Please create or import account first.";
"用户拒绝签名请求。" = "User denied transaction signature.";
"目的地地址错误。" = "Incorrect destination address.";

"签名有误" = "Signature is invalid.";
"目的地账号有误" = "Incorrect destination address.";
"发送方账号不存在" = "Sender account not exist.";
"余额不足" = "Insufficient balance.";
"矿工费不足" = "In sufficient miner fees.";
"第三方dapp合约异常" = "The third-party dapp smart-contract error.";
"账户已激活" = "The account has been registered.";
"请勿重复提交交易" = "Please do not submit txn repeadly.";
"请使用最新区块高度进行交易" = "Please use latest block height in your txn.";
"游戏" = "Games";
"工具" = "Tools";
"其他" = "Others";

"维基时代" = "WaykiTimes";
"维基链，区块链，你想了解的这里都有" = "Here is everything you want to know about WaykiChain.";

"更新提示" = "Release Notes";
"取消" = "Cancel";
"确认" = "Confirm";
"新版本上线" = "New Version";
"当前已是最新版本" = "Currently on the latest version";
"网络出错了～" = "No Internet access";

"参数错误" = "Incorrect parameter";
"发布者" = "Sender";


"代币简称" = "Token Symbol";
"代币全称" = "Token Name";
"总发行量" = "Total Supply";
"代币持有者" = "Token Owner";
"手续费" = "Fees";
"(新)" = "(New)";
"资产发布" = "Issue Asset";
"资产更新" = "Update Asset";
"可修改" = "Modifiable";
"不可修改" = "Unmodifiable";

"Owner账号不存在或者未激活" = "Owner account does not exist or is unregistered";
"不再提醒" = "No prompt";

"CDP创建地址" = "Created by";
"抵押量" = "Collateral amount";
"贷出量" = "Loan amount";
"CDP创建" = "CDP-Inital Stake";
"CDP追加" = "CDP-Further Stake";
"CDP赎回" = "CDP-Redeem";
"CDP清算" = "CDP-Liquidate";
"取消交易" = "Cancel order";
"登录授权" = "Login authorization";
"市价买入" = "Market buy order";
"市价卖出" = "Market sell order";
"限价买入" = "Limit buy order";
"限价卖出" = "Limit sell order";
"取消挂单" = "Cancel order";
"您的身份认证有误，需要重新认证" = "Your identity is incorrect and you need to be verified again";
"重新认证" = "Re-verification";
"消息:" = "Message";
"授权账号:" = "Authorized Address:";
"获取您的钱包地址信息" = "Retrieve your account information";
"签名消息" = "Signature";
"获得" = "Amount received";
"花费" = "Cost";
"价格" = "Price";
"数量" = "Amount";
"类型" = "Type";
"订单号" = "Order";
"预计获得" = "Est. amount received";
"以实际成交为准" = "Subject to actual tx";
"获得数量" = "Amount received";
"清算量" = "Liquidation amount";
"CDP创建交易哈希" = "CDP Create txid";
"清算人地址" = "Liquidator Address";
"赎回量" = "Redemption amount";
"归还量" = "Repay amount";
"申请" = "Apply";
"追加贷出量" = "Further borrow amount";
"追加抵押量" = "Further collateralize amount";
"提示：系统将以当前市场最优价格成交" = "Note: Will transact with the best price in current market.";
"身份授权仅支持中文模式下使用" = "Identity-based authorization is only supported in Chinese language mode";
"信息授权" = "Information authorization";
"获取您的身份证信息" = "Acquire your ID information";
"获取您的手机号信息" = "Acquire your phone number";
"刷新" = "Reload";